class Mover{
  PVector location;
  PVector velocity;
  PVector acceleration;
  float mass;
  
  Mover(float m, float x, float y){
    location = new PVector(x, y);
    velocity = new PVector(0,0);
    acceleration = new PVector(0,0);
    mass = m;
  }
  
  void update(){
    velocity.add(acceleration);
    location.add(velocity);
    acceleration.mult(0);
  }
  void applyForce(PVector force){
    PVector f = force.get();
    f.div(mass);
    acceleration.add(f);
  }
  void display(PGraphics texture){
    texture.stroke(0);
    texture.fill(175);
    texture.ellipse(location.x, location.y, mass*16, mass*16);
  }
  
  void checkEdges(){
    if(location.x > width){
      location.x = width;
      velocity.x *= -1;
    }else if(location.x < 0){
      location.x = 0;
      velocity.x *= -1;
    }
    if(location.y > height){
       location.y = height;
        velocity.y *= -1;
    }else if(location.y < 0){
       velocity.y *= -1;
       location.y = 0;
    }
  }
  
  
}
