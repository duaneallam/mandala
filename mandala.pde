/*
 Mental mandalas prototype
 will draw a 6-axis pattern when the user clicks the mouse in the processing window
 mouse position affects how the mandala is drawn
*/
import neurosky.*;
ThinkGearSocket neuroSocket;

PGraphics texture, canvas, graph, demoCanvas;
PGraphics innerGlyph, outerGlyph, glyphCanvas;
int particles = 600;
int steps = 255;
int innerAxes = 4;
float innerRotate = 0;
int outerAxes = 8;
float outerRotate = 0;
int hue = 0;
int textureSize = 2048;
int glyphSize = 1024;
PImage img, mask;
int translateX, translateY, translateZ = 0;
float rotateX, rotateY, rotateZ = 0;
float scale = 1.0;
JSONObject state;
float meditation, attention = 0;
float delta, theta, low_alpha, high_alpha, low_beta, high_beta, low_gamma, mid_gamma = 0;
float[] brainWaves = new float[8];
boolean poorSignal;
int signalStr;
Mover[] controlPoints = new Mover[8];
boolean neuroMode = false, showDemo = false, showWall = true, showGlyphs = false;
PFont font;
String output;
PVector[] targets = new PVector[8];

void setup(){
  size(1920,1080, P3D);
  
  colorMode(HSB,255);
  
  demoCanvas = createGraphics(glyphSize, glyphSize); 
  demoCanvas.beginDraw();
  demoCanvas.colorMode(RGB,255);
  demoCanvas.endDraw();
  
  outerGlyph = createGraphics(glyphSize, glyphSize); 
  outerGlyph.beginDraw();
  outerGlyph.colorMode(HSB,255);
  outerGlyph.endDraw();
  
  innerGlyph = createGraphics(glyphSize, glyphSize);
  innerGlyph.beginDraw();
  innerGlyph.colorMode(HSB,255);
  innerGlyph.endDraw();
  
  glyphCanvas = createGraphics(glyphSize, glyphSize);
  glyphCanvas.beginDraw();
  glyphCanvas.colorMode(HSB,255);
  glyphCanvas.endDraw();
  
  texture = createGraphics(textureSize, textureSize,P3D);
  texture.beginDraw();
  texture.colorMode(HSB,255);
  texture.background(0);
  texture.endDraw();
  
  graph = createGraphics(200, 200,P3D);
  
  
  img = loadImage("template.png");
  mask = loadImage("mask.png");
  
  translateX = width/2;
  translateY = height/2;
  translateZ = -512;
  //translate(width/2, height/2);
  state = new JSONObject();
  
  font = loadFont("Courier-12.vlw");
  textFont(font);
  /* headset setup */

  ThinkGearSocket neuroSocket = new ThinkGearSocket(this);
  try {
    neuroSocket.start();
  } 
  catch (Exception e) {
    //println("Is ThinkGear running??");
  }
  for(int i = 0; i < controlPoints.length; i++){
    controlPoints[i] = new Mover(1,random(glyphSize),random(glyphSize));
  }
  for(int i = 0; i < targets.length; i++){
    targets[i] = new PVector(0,0);
  }
}

void draw(){
  updateControlPoints();
  //background(128);
  drawGlyphs();
  
  // fade out previous frames - need to fix ghosting
  //fill(0,5);
  //rect(-1,-1,width+2,height+2);
  //noFill();
  //set origin to centre
  texture.beginDraw();
  //texture.background(0);
  texture.translate(textureSize/2,textureSize/2);
  
    texture.background(0);  
    texture.pushMatrix();
    texture.rotate(radians(outerRotate));
    texture.stroke(0,0);
    for(int i = 0; i < outerAxes; i++){
      placeGlyph(outerGlyph, texture, glyphSize, textureSize);
      texture.rotate(TWO_PI/outerAxes);
    }
    texture.scale(-1,1); //invert along x axis
    for(int i = 0; i < outerAxes; i++){
      placeGlyph(outerGlyph, texture, glyphSize, textureSize);
      texture.rotate(TWO_PI/outerAxes);
    }
    texture.popMatrix();
    texture.pushMatrix();
    texture.rotate(radians(innerRotate));
    //inner pattern - use complimentary colour to outer pattern
    for(int i = 0; i < innerAxes; i++){
      placeGlyph(innerGlyph, texture, glyphSize, textureSize);
      texture.rotate(TWO_PI/innerAxes);
    }
    texture.scale(-1,1); //invert along x axis
    for(int i = 0; i < innerAxes; i++){
      placeGlyph(innerGlyph, texture, glyphSize, textureSize);
      texture.rotate(TWO_PI/innerAxes);
    }
    texture.popMatrix();
    hue++;
  innerRotate+=0.05;
  outerRotate+=0.05;
  
  if(hue == 256){
    hue = 0;
  }
  texture.endDraw();
  
  
  background(128,255);
  pushMatrix();
    translate(translateX,translateY,translateZ);
    rotateX(radians(rotateX));
    rotateY(radians(rotateY));
    scale(scale);
    beginShape();
      texture(texture);
      vertex(-512, -512, 0, 0, 0);
      vertex(512, -512, 0, textureSize, 0);
      vertex(512, 512, 0, textureSize, textureSize);
      vertex(-512, 512, 0, 0, textureSize);
    endShape();
  popMatrix();
  
  //image(canvas,0,0);
  if(showWall){
    image(img,0,0);
  }
  fill(255);
  if(neuroMode){
    output = "Mode: Neuro\n"; 
    
  }else{
    output = "Mode: Random\n"; 
  }
  output += "Framerate: "+ frameRate +"\n";
  output += "inner Axes: "+ innerAxes  +"\n";
  output += "outer Axes: "+ outerAxes +"\n";
  
    output += "Signal:\nMed:\nAttn:\n";
    
  textAlign(LEFT, TOP);
  text(output,20,20);
  text("\u03B4\n\u03B8\n\u03B1-\n\u03B1+\n\u03B2-\n\u03B2+\n\u03B3-\n\u03B3+",200,20);
  drawGraph();
  beginShape();
    texture(graph);
    stroke(0,0);
    vertex(200, 20, 0, 0);
    vertex(400, 20, 200, 0);
    vertex(400, 220, 200, 200);
    vertex(200, 220, 0, 200);
  endShape();
  rect(60, 76, map(signalStr, 0, 200, 0,100) , 8);
  rect(60, 90, meditation, 8);
  rect(60, 104, attention, 8);
  
  if(showDemo){
    drawDemo();
    pushMatrix();
    translate(350,200);
    beginShape();
      texture(demoCanvas);
      stroke(0,0);
      vertex(0, 0, 0, 0);
      vertex(512, 0, glyphSize, 0);
      vertex(512, 512, glyphSize, glyphSize);
      vertex(0, 512, 0, glyphSize);
    endShape();
    popMatrix();
  }
  if(showGlyphs){
    colorMode(RGB,255);
    pushMatrix();
    translate(862,200);
    fill(0);
    rect(0,0,1024,512);
    //magenta icon
    fill(255,0,255);
    rect(0,-16,16,16);
    beginShape();
      texture(innerGlyph);
      stroke(0,0);
      vertex(0, 0, 0, 0);
      vertex(512, 0, glyphSize, 0);
      vertex(512, 512, glyphSize, glyphSize);
      vertex(0, 512, 0, glyphSize);
    endShape();
    //green icon
    
    translate(512,0);
    fill(0,255,0);
    rect(0,-16,16,16);
    beginShape();
      texture(outerGlyph);
      stroke(0,0);
      vertex(0, 0, 0, 0);
      vertex(512, 0, glyphSize, 0);
      vertex(512, 512, glyphSize, glyphSize);
      vertex(0, 512, 0, glyphSize);
    endShape();
    popMatrix();
  }
}
void drawShape(float x1,float x2,float x3,float x4,float y1,float y2,float y3,float y4, PGraphics texture){
  texture.translate(texture.width/2,texture.height/2);
  texture.beginShape();
  for (int j = 0; j <= particles; j++) {
    float t = j / float(particles);
    float x = bezierPoint(x1, x2, x3, x4, t);
    float y = bezierPoint(y1, y2, y3, y4, t);
    texture.point(x,y);
  }
  texture.endShape();
}
void drawGlyphs(){
  float x1a,x2a,x3a,x4a,y1a,y2a,y3a,y4a,x1b,x2b,x3b,x4b,y1b,y2b,y3b,y4b,p;
  p = frameCount / 100.0;
  
  x1a = controlPoints[0].location.x;
  x2a = controlPoints[1].location.x;
  x3a = controlPoints[2].location.x;
  x4a = controlPoints[3].location.x;
  y1a = controlPoints[0].location.y;
  y2a = controlPoints[1].location.y;
  y3a = controlPoints[2].location.y;
  y4a = controlPoints[3].location.y;
  //control points for magenta pattern
  x1b = controlPoints[4].location.x;
  x2b = controlPoints[5].location.x;
  x3b = controlPoints[6].location.x;
  x4b = controlPoints[7].location.x;
  y1b = controlPoints[4].location.y;
  y2b = controlPoints[5].location.y;
  y3b = controlPoints[6].location.y;
  y4b = controlPoints[7].location.y;
  
  outerGlyph.beginDraw();
  outerGlyph.stroke(hue,255,255,20);
  //translate to where the mouse pointer is on sc
  drawShape(x1a,x2a,x3a,x4a,y1a,y2a,y3a,y4a,outerGlyph);
  outerGlyph.endDraw();
  innerGlyph.beginDraw();
  innerGlyph.stroke((hue+128)%255,255,255,20);
  drawShape(x1b,x2b,x3b,x4b,y1b,y2b,y3b,y4b,innerGlyph);
  innerGlyph.endDraw();
};
void placeGlyph(PGraphics glyph,PGraphics texture,int glyphSize,int textureSize){
  texture.beginShape();
    texture.texture(glyph);
    texture.vertex(0, 0, 0, 0, 0);
    texture.vertex(textureSize/2, 0, 0, glyphSize, 0);
    texture.vertex(textureSize/2, textureSize/2, 0, glyphSize, glyphSize);
    texture.vertex(0, textureSize/2, 0, 0, glyphSize);
  texture.endShape();
}
void updateControlPoints(){
  float p = frameCount / 500.0;
  PVector force, dir, target;
  float targetX, targetY;
  for(int i = 0; i < controlPoints.length; i++){
    if(i < 4){
      if(neuroMode){
        targets[i].x = map(brainWaves[i * 2],0,15,-glyphSize/2,glyphSize/2);
        targets[i].y = map(brainWaves[(i * 2) + 1],0,15,-glyphSize/2,glyphSize/2);
      }else{
        targets[i].x = map(noise(i + p), 0,1,-glyphSize/2,glyphSize/2);
        targets[i].y = map(noise(i*2 + p), 0,1,-glyphSize/2,glyphSize/2);
      }
      
    }else{
      if(neuroMode){
        targets[i].x = map(brainWaves[((controlPoints.length -1 - i))* 2],0,15,-glyphSize/3,glyphSize/3);
        targets[i].y = map(brainWaves[(((controlPoints.length -1 - i))* 2) + 1],0,15,-glyphSize/3,glyphSize/3);
      }else{
        targets[i].x = map(noise(i + p), 0,1,-glyphSize/3,glyphSize/3);
        targets[i].y = map(noise(i*2 + p), 0,1,-glyphSize/3,glyphSize/3);
      }
    }
      dir = PVector.sub(targets[i],controlPoints[i].location);
      dir.normalize();
      dir.mult(0.05);
   
      controlPoints[i].applyForce(dir);
      controlPoints[i].update();
  }
}
void drawGraph(){
  String[] labels = {"\u03B4","\u03B8","\u03B1-","\u03B1+","\u03B2-","\u03B2+","\u03B3-","\u03B3+"};
  graph.beginDraw();
    graph.clear();
    for(int i = 0; i < 8; i++){
      graph.stroke(0,0);
      graph.fill(255);
      graph.rect(20,i*14,brainWaves[i],8);
      
    }
  graph.endDraw();
  
}
void drawDemo(){
  
    float x1a,x2a,x3a,x4a,y1a,y2a,y3a,y4a,x1b,x2b,x3b,x4b,y1b,y2b,y3b,y4b,p;
    p = frameCount / 100.0;
    
    x1a = controlPoints[0].location.x;
    x2a = controlPoints[1].location.x;
    x3a = controlPoints[2].location.x;
    x4a = controlPoints[3].location.x;
    y1a = controlPoints[0].location.y;
    y2a = controlPoints[1].location.y;
    y3a = controlPoints[2].location.y;
    y4a = controlPoints[3].location.y;
    //control points for magenta pattern
    x1b = controlPoints[4].location.x;
    x2b = controlPoints[5].location.x;
    x3b = controlPoints[6].location.x;
    x4b = controlPoints[7].location.x;
    y1b = controlPoints[4].location.y;
    y2b = controlPoints[5].location.y;
    y3b = controlPoints[6].location.y;
    y4b = controlPoints[7].location.y;
  
  demoCanvas.beginDraw();
  demoCanvas.translate(glyphSize/2,glyphSize/2);
    demoCanvas.background(0);
    for(int i = 0; i < controlPoints.length; i++){
      demoCanvas.ellipseMode(RADIUS);
      demoCanvas.stroke(0, 0, 0);
      demoCanvas.fill(255,0,0);
      demoCanvas.ellipse(controlPoints[i].location.x,controlPoints[i].location.y,5,5);
      demoCanvas.fill(0,255,255);
      demoCanvas.ellipse(targets[i].x,targets[i].y,5,5);
      demoCanvas.stroke(128.128);
      demoCanvas.line(controlPoints[i].location.x,controlPoints[i].location.y, targets[i].x,targets[i].y);
      
      
      demoCanvas.noFill();
      demoCanvas.stroke(0, 128, 0);
      demoCanvas.line(x1a, y1a, x2a, y2a);
      demoCanvas.line(x3a, y3a, x4a, y4a);
      demoCanvas.stroke(0, 255, 0);
      demoCanvas.bezier(x1a, y1a, x2a, y2a, x3a, y3a, x4a, y4a);
      
      demoCanvas.stroke(128, 0, 128);
      demoCanvas.line(x1b, y1b, x2b, y2b);
      demoCanvas.line(x3b, y3b, x4b, y4b);
      demoCanvas.stroke(255, 0, 255);
      demoCanvas.bezier(x1b, y1b, x2b, y2b, x3b, y3b, x4b, y4b);
    }
  demoCanvas.endDraw();
}

void stop() {
  neuroSocket.stop();
  super.stop();
}
