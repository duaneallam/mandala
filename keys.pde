void keyPressed() {
  if(key == 'r'){
    innerGlyph.beginDraw();
    innerGlyph.clear();
    innerGlyph.endDraw();
    outerGlyph.beginDraw();
    outerGlyph.clear();
    outerGlyph.endDraw();
  }
  
  if(key == '['){
    scale -= 0.1;
  }
  if(key == ']'){
    scale += 0.1;
  }
  if(key == '='){
    innerAxes++;
  }
  if(key == '-'){
    innerAxes--;
  }
  if(key == '+'){
    outerAxes++;
  }
  if(key == '_'){
    outerAxes--;
  }
  if(key == 's'){
    state.setInt("translateX",translateX);
    state.setInt("translateY",translateY);
    state.setInt("translateZ",translateZ);
    state.setFloat("rotateX",rotateX);
    state.setFloat("rotateY",rotateY);
    state.setFloat("rotateZ",rotateZ);
    state.setFloat("scale",scale);
    saveJSONObject(state,"data/state.json");
  }
  if(key == 'l'){
    state = loadJSONObject("data/state.json");
    translateX = state.getInt("translateX");
    translateY = state.getInt("translateY");
    translateZ = state.getInt("translateZ");
    rotateX = state.getFloat("rotateX");
    rotateY = state.getFloat("rotateY");
    rotateZ = state.getFloat("rotateZ");
    scale = state.getFloat("scale");

  }
  if(key == 'm'){
    innerGlyph.beginDraw();
    innerGlyph.clear();
    innerGlyph.endDraw();
    outerGlyph.beginDraw();
    outerGlyph.clear();
    outerGlyph.endDraw();
    neuroMode = !neuroMode;
  }
  if(key == 'd'){
    showDemo = !showDemo;
  }
  if(key == 'w'){
    showWall = !showWall;
  }
  if(key == 'g'){
    showGlyphs = !showGlyphs;
  }
}
void mouseDragged() {
 if ( mouseButton == LEFT) {
  rotateX = rotateX + (mouseY - pmouseY)/10;
  rotateY = rotateY + (mouseX - pmouseX)/10;
 }else if(mouseButton == RIGHT){
   translateX = translateX + (mouseX - pmouseX);
   translateY = translateY + (mouseY - pmouseY);
 }
}
