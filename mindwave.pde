public void attentionEvent(int attentionLevel) {
  //println("Attention Level: " + attentionLevel + ", millis: " + millis());
  attention = attentionLevel;
}


void meditationEvent(int meditationLevel) {
  //println("Meditation Level: " + meditationLevel);
  meditation = meditationLevel;
}

public void eegEvent(int deltaLevel, int thetaLevelLevel, int low_alphaLevel, int high_alphaLevel, int low_betaLevel, int high_betaLevel, int low_gammaLevel, int mid_gammaLevel) {
  brainWaves[0] = delta = log(deltaLevel);
  brainWaves[1] = theta = log(thetaLevelLevel);
  brainWaves[2] = low_alpha = log(low_alphaLevel);
  brainWaves[3] = high_alpha = log(high_alphaLevel);
  brainWaves[4] = low_beta = log(low_betaLevel);
  brainWaves[5] = high_beta = log(high_betaLevel);
  brainWaves[6] = low_gamma = log(low_gammaLevel); 
  brainWaves[7] = mid_gamma = log(mid_gammaLevel);
}

void poorSignalEvent(int sig) {
  println("SignalEvent "+sig);
  if (sig > 0) {
    poorSignal = true;
  } else {
    poorSignal = false;
  }
  signalStr = 200 - sig;
}
void rawEvent(int[] raw) {
  //  println("raw.length: " + raw.length + ", millis: " + millis());
  //  println("rawEvent Level: " + raw);
  //  for(int i = 0; i < raw.length; i++) {
  //    println(raw[i]);
  //  }
}  
void blinkEvent(int blinkStrength) {
  println("Blink Strength: " + blinkStrength);
}
